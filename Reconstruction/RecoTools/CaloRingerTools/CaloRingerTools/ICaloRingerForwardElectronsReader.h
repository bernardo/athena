/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: ICaloRingerPhotonsReader.h 667886 2015-05-18 17:26:59Z wsfreund $
#ifndef CALORINGERTOOLS_ICALORINGERFORWARDELECTRONSREADER
#define CALORINGERTOOLS_ICALORINGERFORWARDELECTRONSREADER

/**
   @class ICaloRingerForwardElectronsReader
   @brief Interface for tool CaloRingerForwardReader

   @author Werner S. Freund <wsfreund@cern.ch>
   @author Juan L. Marin <juan.lieber.marin@cern.ch>

   $Revision: 667886 $
   $$$
*/

// Core Include
#include "GaudiKernel/IAlgTool.h"

// Interface Includes:
#include "ICaloRingerInputReader.h"

namespace Ringer {

static const InterfaceID IID_ICaloRingerForwardElectronsReader("ICaloRingerForwardElectronsReader", 1, 0);

class ICaloRingerForwardElectronsReader : virtual public ICaloRingerInputReader
{
 public:
  /** @brief Virtual destructor*/
  virtual ~ICaloRingerForwardElectronsReader() {};
	
  /** @brief AlgTool interface methods */
  static const InterfaceID& interfaceID();

  /** @brief initialize method*/
  virtual StatusCode initialize() = 0;
  /** @brief execute method **/                                                    
  virtual StatusCode execute() = 0;
  /** @brief finalize method*/
  virtual StatusCode finalize() = 0;

};

inline const InterfaceID& ICaloRingerForwardElectronsReader::interfaceID()
{
  return IID_ICaloRingerForwardElectronsReader;
}

} // namespace Ringer

#endif

