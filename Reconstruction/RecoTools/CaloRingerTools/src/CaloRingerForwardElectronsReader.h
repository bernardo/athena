/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// $Id: CaloRingerPhotonsReader.h 752568 2016-06-03 16:03:21Z ssnyder $
#ifndef CALORINGERTOOLS_CALORINGERFORWARDELECTRONSREADER_H
#define CALORINGERTOOLS_CALORINGERFORWARDELECTRONSREADER_H

// STL includes:
#include <string>

// Base includes:
#include "CaloRingerInputReader.h"
#include "CaloRingerTools/ICaloRingerForwardElectronsReader.h"
#include "CaloRingerReaderUtils.h"

// xAOD includes:
#include "xAODEgamma/ElectronContainerFwd.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"


// Asg selectors include:
#include "RingerSelectorTools/IAsgElectronRingerSelector.h"

#include "StoreGate/ReadHandleKey.h"

namespace Ringer {

class CaloRingerForwardElectronsReader : public CaloRingerInputReader,
                                public ICaloRingerForwardElectronsReader
{

  public:

    /// @name CaloRingerForwardElectronsReader ctors and dtors:
    /// @{
    /**
     * @brief Default constructor
     **/
    CaloRingerForwardElectronsReader(const std::string& type,
                     const std::string& name,
                     const ::IInterface* parent);

    /**
     * @brief Destructor
     **/
    ~CaloRingerForwardElectronsReader();
    /// @}

    /// Tool main methods:
    /// @{
    /**
     * @brief initialize method
     **/
    virtual StatusCode initialize() override;
    /**
     * @brief read electrons and populates @name decoMap with them and their
     * respective CaloRings.
     **/
    virtual StatusCode execute() override;
    /**
     * @brief finalize method
     **/
    virtual StatusCode finalize() override;
    /// @}


  private:

    /// Tool CaloRingerElectronsReader props (python configurables):
    /// @{
    /**
     * @brief Electron selectors.
     * TODO Change it to Forward Electrons Selector
     **/
    PublicToolHandleArray<IAsgElectronRingerSelector> m_ringerSelectors {this,
	"ForwardElectronSelectors", {}, "The ASG Forward Electron Selectors."};

    /**
     * @brief Hold selectors result names.
     **/
    Gaudi::Property<std::vector<std::string> > m_ringerSelectorResultNames {this,
	"ResultNames", {}, "The ASG Selectors result names."};


    /** @brief forward electron collection input name*/
    SG::ReadHandleKey<xAOD::ElectronContainer> m_inputElectronContainerFwdKey {this,
      "inputKey",
      "ElectronFwd",
      "Name of the input electron forward container"};
    /// @}


    /// Tool CaloRingerForwardsReader props (non configurables):
    /// @{

    /// The CaloRings Builder functor:
    BuildCaloRingsFctor<xAOD::ElectronContainer> *m_clRingsBuilderForwardElectronFctor;

    /// Whether selectors are available
    //bool m_selectorAvailable;
    /// @}

};

}

#endif
