/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// $Id: CaloRingerForwardElectronsReader.cxx 752568 2016-06-03 16:03:21Z ssnyder $
// =============================================================================
#include "CaloRingerForwardElectronsReader.h"
#include "StoreGate/ReadHandle.h"

#include <algorithm>

namespace Ringer {

// =============================================================================
CaloRingerForwardElectronsReader::CaloRingerForwardElectronsReader(const std::string& type,
                                 const std::string& name,
                                 const ::IInterface* parent)
  : CaloRingerInputReader(type, name, parent),
    m_clRingsBuilderForwardElectronFctor(0)
    //m_selectorAvailable(false)
{

  // declare interface
  declareInterface<ICaloRingerForwardElectronsReader>(this);

}

// =============================================================================
CaloRingerForwardElectronsReader::~CaloRingerForwardElectronsReader()
{
  if(m_clRingsBuilderForwardElectronFctor) delete m_clRingsBuilderForwardElectronFctor;
}

// =============================================================================
StatusCode CaloRingerForwardElectronsReader::initialize()
{

  ATH_CHECK( CaloRingerInputReader::initialize() );

  ATH_CHECK(m_inputElectronContainerFwdKey.initialize());

  if ( m_builderAvailable ) {
    // Initialize our fctor
    m_clRingsBuilderForwardElectronFctor =
      new BuildCaloRingsFctor<xAOD::ElectronContainer>(
          m_inputElectronContainerFwdKey.key(),
          m_crBuilder,
          msg(),
	  this);
      ATH_CHECK( m_clRingsBuilderForwardElectronFctor->initialize() );
  }

  return StatusCode::SUCCESS;
}

// =============================================================================
StatusCode CaloRingerForwardElectronsReader::finalize()
{
  return StatusCode::SUCCESS;
}

// =============================================================================
StatusCode CaloRingerForwardElectronsReader::execute()
{

  ATH_MSG_DEBUG("Entering " << name() << " execute.");

  // Retrieve photons
  SG::ReadHandle<xAOD::ElectronContainer> electronsfwd(m_inputElectronContainerFwdKey);
  // check is only used for serial running; remove when MT scheduler used
  if(!electronsfwd.isValid()) {
    ATH_MSG_FATAL("Failed to retrieve "<< m_inputElectronContainerFwdKey.key());
    return StatusCode::FAILURE;
  }
  if ( m_builderAvailable ) {
    ATH_CHECK( m_clRingsBuilderForwardElectronFctor->prepareToLoopFor(electronsfwd->size()) );

    // loop over our particles:
    for ( const auto electronfwd : *electronsfwd ){
      m_clRingsBuilderForwardElectronFctor->operator()( electronfwd );
    }

  }


  return StatusCode::SUCCESS;

}

} // namespace Ringer

